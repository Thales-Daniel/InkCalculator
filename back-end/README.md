# Boas vindas ao repositório Ink Calculator!


Este repositório tem como objetivo criar um aplicação Full-stack, onde sera criado Uma aplicação web ou mobile que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede.
Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L


# Sumário
- [Instruções](#instruções)
- [Tecnologias](#tecnologias)
- [Executando o projeto](#executando-o-projeto)
- [Proximos passos](#proximos-passos)
- [Principal Desafio](#desafio-principal)
- [Testes](#testes)


# Instruções:

Inicie clonando o repositorio para sua máquina local:
~~~
git clone git@gitlab.com:Thales-Daniel/inkCalculator-BackEnd.git
~~~
Entre na pasta do repositorio:
~~~
cd inkCalculator-BackEnd
~~~
Instale as dependencias:
~~~
npm install
~~~
E para finalmente iniciar o projeto, é necessário o comando a baixo:
~~~
npm start
~~~

OBSERVAÇOES:

É necessario também entrar na pasta do back-end e criar o arquivo
.env, assim como o .envExample.


E para executar os testes é so executar o comando:
~~~
npm test
~~~

# Tecnologias

As tecnologias para desenvolver a API foram as seguintes:

  - `Node.js`
  - `Express`
  - `Eslint`
  - `Jest`
  - `Typescript`
  - `Super Test`
  - `Dotenv`
  - `Http Status Codes`


<div align="center">
  <img alt="Node" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-original.svg" />
  <img alt="express" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/express/express-original.svg" />
  <img alt="eslint" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/eslint/eslint-original.svg" />
  <img alt="jest" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jest/jest-plain.svg" />
  <img alt="typescript" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg" />
  <br />
  <br />
</div>
  <br />
  <br />


# Executando o projeto
<div align="center">
  <img  src="./src/assets/inkCalculator.png" />
  <img  src="./src/assets/m2Conversor.png" />
  <br />
  <br />
</div>


# Testes

Os testes de integração foram desenvolvidos com jest e supertests

<div>
  <img src="./src/assets/testes.png" />
<div>
  

# Desafio Principal
  
O principal desafio do projeto foi organizar por onde seria iniciado a produção, alem disso, houveram problemas na hora da criação dos testes que
causou um atraso grande.



# Proximos passos

O proximo passo seria aplicar o docker e alem disso
refatorar a API para POO.