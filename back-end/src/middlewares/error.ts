import { ErrorRequestHandler } from 'express';
import { StatusCodes } from 'http-status-codes';

const error: ErrorRequestHandler = (err, _req, res, _next) => {
  console.error(err);

  res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ err });
};

export default error;
