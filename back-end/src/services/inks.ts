import closestNumber from '../helpers/closestNumber';
import stringMaker from '../helpers/stringMaker';
import { InksQuantity, IInkCalculator } from '../types';

function inkCalculator(area: number) : IInkCalculator {
  const inkLitres = +(area / 5).toFixed(2);
  const inks = [0.5, 2.5, 3.6, 18.0];

  const quantityHashmap = {} as InksQuantity;
  let quantity = 0;
  let closest = closestNumber(inkLitres, inks);
  let diff = 0;

  while (quantity < inkLitres) {
    diff = inkLitres - quantity;
    closest = closestNumber(diff, inks);
    quantity += closest;
    if (!quantityHashmap[closest as keyof InksQuantity]) {
      quantityHashmap[closest as keyof InksQuantity] = 1;
    } else {
      quantityHashmap[closest] += 1;
    }
  }

  const result = { stringWithInks: stringMaker(quantityHashmap), inks: quantityHashmap };
  return result;
}

export default inkCalculator;
