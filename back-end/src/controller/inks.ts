import { StatusCodes } from 'http-status-codes';
import { Handler } from '../types';
import inkService from '../services/inks';

const inkCalculator : Handler = async (req, res, next) => {
  try {
    const { area } = req.body;

    const getInk = inkService(area);

    return res.status(StatusCodes.OK).json(getInk);
  } catch (e) {
    next(e);
  }
};

export default inkCalculator;
