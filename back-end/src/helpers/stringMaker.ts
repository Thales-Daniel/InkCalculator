import { InksQuantity } from '../types';

function stringMaker(obj: InksQuantity): string {
  const objValue = Object.values(obj);
  const objKeys = Object.keys(obj);

  const total = objValue.reduce((acc, prev, _curr) => acc + +prev);
  const lt = total > 1 ? 'cans' : 'tin';

  switch (objValue.length) {
    case 1:
      return `You need a total of ${total} ${lt}: ${objValue[0]} of ${objKeys[0]} Litros`;
    case 2:
      return `You need a total of ${total} ${lt}: ${objValue[1]} of ${objKeys[1]} Liters and ${objValue[0]} of ${objKeys[0]} Liters`;
    case 3:
      return `You need a total of ${total} ${lt}: ${objValue[1]} of ${objKeys[1]} Liters, ${objValue[0]} of ${objKeys[0]} Liters and ${objValue[2]} of ${objKeys[2]} Liters`;
    case 4:
      return `You need a total of ${total} ${lt}: ${objValue[1]} of ${objKeys[1]} Liters, ${objValue[0]} of ${objKeys[0]} Liters, ${objValue[2]} of ${objKeys[2]} Liters and ${objValue[3]} of ${objKeys[3]} Liters`;
    default:
      return 'You don`t need any cans';
  }
}

export default stringMaker;
