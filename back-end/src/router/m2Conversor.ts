import express from 'express';
import m2ConversorController from '../controller/m2Conversor';

const router = express.Router({ mergeParams: true });

router.post('/', m2ConversorController);

export default router;
