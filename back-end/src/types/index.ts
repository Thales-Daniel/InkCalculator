import { NextFunction, Request, Response } from 'express';

export interface IWallType {
  [key: number]: string,
  height: number,
  width: number,
  windows: number,
  doors: number,
}

export interface InksQuantity {
  [key: string]: number,
  18: number,
  3.6: number,
  2.5: number,
  0.5: number,
}

export interface IInkCalculator {
  inks: InksQuantity;
  stringWithInks: string;
}

export type Handler = (_req: Request, _res: Response, _next: NextFunction) => void;