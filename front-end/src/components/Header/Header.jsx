import React from 'react';
import './style.css';

function Header() {
  return (
    <header className="header">
      <h1 className="title">
        Ink Calculator
      </h1>
    </header>
  );
}

export default Header;
