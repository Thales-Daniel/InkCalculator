import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import './style.css';

function Wall({ setMeasurements, setVerify, name }) {
  const [height, setHeight] = useState('');
  const [doors, setDoors] = useState('');
  const [windows, setWindows] = useState('');
  const [width, setWidth] = useState('');
  const verify = height && width && doors && windows;

  useEffect(() => {
    setMeasurements({
      height, doors, windows, width,
    });
    setVerify(Boolean(verify));
  }, [width, windows, doors, height]);

  return (
    <div className="wallsContainer">
      <div className="divTitleWall">
        <h2>{name}</h2>
      </div>
      <div className="measurements">
        <div className="widthAndHeight">
          <label htmlFor="height">
            HEIGHT:
            <input
              className="numberInput"
              type="number"
              id="heigth"
              onChange={({ target }) => setHeight(target.value)}
            />
          </label>
          <label htmlFor="width">
            WIDTH:
            <input
              className="numberInput"
              type="number"
              id="width"
              onChange={({ target }) => setWidth(target.value)}
            />
          </label>
        </div>
        <div className="doorsAndWindows">
          <label htmlFor="windows">
            WINDOWS:
            <input
              className="numberInput"
              type="number"
              id="windows"
              onChange={({ target }) => setWindows(target.value)}
            />
          </label>
          <label htmlFor="doors">
            DOORS:
            <input
              className="numberInput"
              type="number"
              id="doors"
              onChange={({ target }) => setDoors(target.value)}
            />
          </label>
        </div>
      </div>
    </div>
  );
}

Wall.propTypes = {
  setWallsMensurements: PropTypes.func,
}.isRequired;

export default Wall;
