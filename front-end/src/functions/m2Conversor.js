import axios from 'axios';

const m2Conversor = async (measurements) => {
  try {
    const getData = await axios.post('http://localhost:3002/m2Conversor', { measurements });
    return getData.data;
  } catch (err) {
    return err.response.data;
  }
};

export default m2Conversor;
