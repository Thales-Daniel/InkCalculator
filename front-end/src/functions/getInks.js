import axios from 'axios';

const getInks = async (area) => {
  try {
    const getData = await axios.post('http://localhost:3002/inkCalculator', { area });
    return getData.data;
  } catch (err) {
    return err.response.data;
  }
};

export default getInks;
