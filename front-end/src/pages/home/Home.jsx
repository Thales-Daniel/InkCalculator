import React from 'react';
import Footer from '../../components/Footer/Footer';
import Form from '../../components/Form/Form';
import Header from '../../components/Header/Header';
import './style.css';

function Home() {
  return (
    <div className="homeContainer">
      <Header />
      <Form />
      <Footer />
    </div>
  );
}

export default Home;
