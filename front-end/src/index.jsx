import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import './index.css';
import App from './App';
import WallsProvider from './context/WallProvider';

const root = ReactDOM.createRoot(
  document.getElementById('root'),
);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <WallsProvider>
        <App />
      </WallsProvider>
    </BrowserRouter>
  </React.StrictMode>,
);
