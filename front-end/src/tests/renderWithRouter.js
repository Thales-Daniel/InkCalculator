import React from 'react';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import WallsProvider from '../context/WallProvider';

const renderWithRouter = (component, { route = '/' } = {}) => ({
  ...render(
    <MemoryRouter initialEntries={[route]}>
      <WallsProvider>
        {component}
      </WallsProvider>
    </MemoryRouter>,
  ),
});

export default renderWithRouter;
